package guru.springframework.spring6di.controllers.datasource;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles({"qa", "EN"})
@SpringBootTest
class QADataSourceControllerTest extends DataSourceControllerTest{

}
