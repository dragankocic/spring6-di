package guru.springframework.spring6di.controllers.datasource;

import guru.springframework.spring6di.controllers.DataSourceController;
import guru.springframework.spring6di.services.datasource.DataSourceService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles({"prod", "EN"})
@SpringBootTest
class ProductionDataSourceControllerTest extends DataSourceControllerTest{


}