package guru.springframework.spring6di.controllers.datasource;

import guru.springframework.spring6di.controllers.DataSourceController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class DataSourceControllerTest {

    @Autowired
    DataSourceController dataSourceController;

    @Test
    void getDataSource() {
        System.out.println(dataSourceController.getDataSource());
    }
}
