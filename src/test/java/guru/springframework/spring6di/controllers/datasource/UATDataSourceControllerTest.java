package guru.springframework.spring6di.controllers.datasource;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles({"uat", "EN"})
@SpringBootTest
class UATDataSourceControllerTest extends DataSourceControllerTest{


}
