package guru.springframework.spring6di.services.datasource;

public interface DataSourceService {

    public String getDataSource();
}
