package guru.springframework.spring6di.services.datasource;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Profile({"dev", "default"})
@Service
public class DevDataSourceServiceImpl implements DataSourceService {

    @Override
    public String getDataSource() {
        return "Development Data Source";
    }
}
